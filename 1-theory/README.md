# Theory
This is a small test to evaluate how much you know about NODE.

## Instructions

Select the answer you consider correct and write the letter A, B, C, D, or E in the Answer line.

## Questions

### Question 1
Which of the following does NOT describe NPM?

    A) A command-line that helps install node packages.
    B) A node versions manager.
    C) A node package manager.
    D) A node module.
    E) An online Node repository for Node packages.

#### Answer:

### Question 2
Async/await is used in order for the code to behave like synchronous code

    A) True
    B) False

#### Answer:

### Question 3
In javascript is `undefined` equal to `null`?

    A) True
    B) False

#### Answer:

### Question 4
Which of the following statements is false about Node?

    A) Is multi-threaded
    B) Has asynchonous execution
    C) You can create applications that can be scaled quickly
    D) You can use it client-side and server-side
    E) It has a lot of frameworks you can use

#### Answer:

### Question 5
In javascript, a callback is a function passed in as a parameter to be executed later.

    A) True
    B) False

#### Answer:

### Question 6
 Which of the following options defines best the use of `let` and `var`?

    A) "Let" variables can't change their values and "Var" variables can change their values.
    B) "Var" variables can't change their values and "Let" variables can change their values.
    C) Both variables can change their values.
    D) Both variables can't change their values.

#### Answer:

### Question 7
 Which is the name given to the function that can access res and req objects?

    A) Recursive function
    B) Middleware function
    C) Annotated function
    D) Arrow function
    E) Pre defined function

#### Answer:

### Question 8
Can you change the value of a `const` variable?

    A) Yes
    B) No

#### Answer:

### Question 9
What will be the output of the following?

    const object = {};
    object.property = 1;
    console.log(object);

    A) 1
    B) { object: property: 1 }
    C) Uncaught TypeError: Assignment to constant variable.
    D) { property: 1 }

#### Answer:

### Question 10
Which of the following does NOT clone the object? 

    const object = { property: 1};

    A) const clone = object;
    B) const clone = Object.assign({}, object);
    C) const clone = { ...object };
    D) const clone = JSON.parse(JSON.stringify(object));

#### Answer:
