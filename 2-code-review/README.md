# Code Review
Do you know how important the code-review process is? Sadly, many companies don't take care about it. But we are the exception. 

## Instructions

Create a pull request with the code-review recommendations of the code in the `interview.js` file.