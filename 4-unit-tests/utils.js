const sanitizeHtmltags = (str) => {
  if (typeof str === 'string') {
    return str.replace(/<|>/g, '');
  }
  return str;
}

const decodeData = (data) => {
  const url = data.url;
  try {
    return decodeURI(url);
  } catch (e) {
    return '';
  }
};

const normalizeStringQueryParam = (param, defaultValue) => {
  if (!param) {
    return defaultValue;
  }
  if (typeof param !== 'string') {
    const paramTypeError = new Error('Incorrect query param data type');
    paramTypeError.statusCode = 404;
    throw paramTypeError;
  }
  return param;
};

module.exports = {
  sanitizeHtmltags,
  decodeData,
  normalizeStringQueryParam
};