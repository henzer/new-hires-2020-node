# Unit tests
This is the best way to ensure that the code does what it is supposed to do, and to find bugs at an early stage. 

## Instructions
The purpose of this section is to evaluate your knowledge about unit testing.

This test contains a file called `utils.js`. It contains 3 functions. You should add unit tests in order to have code coverage on each function.  

You can use the unit testing framework that you prefer. As an example, a **dummy unit-test** was added on `test/utils.test.js` using `jest`. 
To see the **dummy unit-test** results, you can use:

- Install dependencies: `npm install`
- Run tests: `npm test`
- Coverage: `npm test -- --coverage`

## Things to evaluate (10 pts):
- (3pts) Coverage in `sanitizeHtmltags`
- (3pts) Coverage in `decodeData`
- (4pts) Coverage in `normalizeStringQueryParam`
